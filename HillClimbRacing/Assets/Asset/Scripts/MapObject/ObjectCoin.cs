using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectCoin : MonoBehaviour
{
    private enum ValueCoin
    {
        
        x5,
        x25,
        x100,
        x500

    }

    [SerializeField] private ValueCoin m_valueCoin = ValueCoin.x5;
    [SerializeField] private Sprite[] m_listSprites = new Sprite[4];
    [SerializeField] private SpriteRenderer m_sprite;

    private int m_value = 0;
    // Start is called before the first frame update
    void Start()
    {
        OnInit();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnInit()
    {
        //Debug.Log("====" + int.Parse(m_valueCoin.ToString()));
        int value = 0;
        switch(m_valueCoin)
        {
            case ValueCoin.x5:
                m_value = 5;
                value = 0;
                break;
            case ValueCoin.x25:
                m_value = 25;
                value = 1;
                break;
            case ValueCoin.x100:
                m_value = 100;
                value = 2;
                break;
            case ValueCoin.x500:
                m_value = 500;
                value = 3;
                break;
                
        }
        m_sprite.sprite = m_listSprites[value];
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Player")
        {
            MainController.UpdateCoin(m_value);
            OnDoTrigger();
           
        }
    }
    private void OnDoTrigger()
    {
        transform.DOLocalMoveY(transform.localPosition.y + 2f, 1f).OnUpdate(() => {
            m_sprite.DOFade(0f, 0.5f);
        });
        Destroy(gameObject,2f);
    }
}
