using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ObjectDiamond : MonoBehaviour
{
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Player")
        {
            MainController.UpdateDiamond(1);
            OnDoTrigger();
        }
    }
    private void OnDoTrigger()
    {
        transform.DOLocalMoveY(transform.localPosition.y + 2f, 0.5f).OnUpdate(() => {
            transform.GetComponent<SpriteRenderer>().DOFade(0f, 0.5f);
        });
        Destroy(gameObject,2f);
    }
}
