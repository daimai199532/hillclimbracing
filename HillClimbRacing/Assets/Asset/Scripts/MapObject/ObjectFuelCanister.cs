using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectFuelCanister : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_sprite;
    private void OnEnable()
    {
        
    }
    private void Start()
    {
        MainController.UpdateFuelCanisterStartGame(transform.position.x, true);
    }
    private void OnDestroy()
    {
        MainController.UpdateFuelCanisterStartGame(transform.position.x, false);
    }
    // Start is called before the first frame update
    private void OnTrigger()
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOMoveY(transform.position.y + 2f, 0.5f));
        seq.Join(m_sprite.DOFade(0.3f, 0.5f));
        seq.Play();
        Destroy(gameObject, 0.8f);
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Player")
        {
            //Debug.Log("player ==============================");
            MainController.UpdateTime(60);
            OnTrigger();
        }
    }
}
