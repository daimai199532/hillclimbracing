using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelCar : MonoBehaviour
{
    [SerializeField] private Rigidbody2D m_rigidbody2D;
    //[SerializeField] private typeWheel m_typeWheel = typeWheel.right;
    // Start is called before the first frame update
    //private int m_id = 0;
    void Start()
    {
        //if (m_typeWheel == typeWheel.right)
        //    m_id = 2;
        //else
        //    m_id = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision) // check co o tren mat dat ko 
    {
        if(collision.gameObject.tag == "ground")
        {
            MainController.UpdateCheckGroundWheelCar(m_rigidbody2D,true);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            MainController.UpdateCheckGroundWheelCar(m_rigidbody2D, false);
        }
    }

    private enum typeWheel
    {
        left,
        right
    }
}
