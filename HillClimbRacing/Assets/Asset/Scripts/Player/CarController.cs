using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using System;

public class CarController : MonoBehaviour
{
    //[SerializeField] private GameObject m_Target;

    [SerializeField] private float m_force;
    [Range(-5, 5)]
    [SerializeField] private float m_makeEffect;
    [Range(-1, 1)]
    [SerializeField] private float m_makeLeft;
    [SerializeField] private EffectRocks m_effectRock;
    [SerializeField] private Camera m_camera;
    [SerializeField] private Camera m_cameraScreenShot;
    [SerializeField] private Rigidbody2D m_rigidbody2D;
    [SerializeField] Rigidbody2D m_wheelLeft;
    [SerializeField] Rigidbody2D m_wheelRight;
    [Range(0, 1)]
    [SerializeField] private float m_rotate;
    [SerializeField] private int m_speedMovement = 0;

    private WheelJoint2D[] hingeJoints;
    private bool m_leftEnter;
    private bool m_rightEnter;

    private int m_countMove = 0;
    private bool m_isPush = false;
    private float m_timePush = 0;
    private float m_timeDontPush = 0;
    private int m_left = 0;

    private bool m_isMove = false;

    private bool m_groundWheelLeft = false;
    private bool m_groundWheelRight = false;

    private bool m_isActive = false;

    private float m_velocityX = 0;
    [Range(3.5f,10)]
    [SerializeField] private float m_sizeCamera = 3.5f;
    private bool m_isZoomCamera = false;

    private float m_timeInAir = 0;
    private void Awake()
    {
        DOTween.SetTweensCapacity(2000, 100);
        MainController.updateCheckGroundWheelCar += UpdateCheckGroundWheel;
        MainController.gameOverEvent += GameOver;
    }
    private void OnDestroy()
    {
        MainController.updateCheckGroundWheelCar -= UpdateCheckGroundWheel;
        MainController.gameOverEvent -= GameOver;
    }
    // Start is called before the first frame update
    void Start()
    {
        //MakeEffectCount(1000, m_rigidbody2D.velocity);
        hingeJoints = GetComponents<WheelJoint2D>();
        m_velocityX = transform.position.x;
        m_sizeCamera = 3.5f;
        //ZoomCamera(false);
    }

    // Update is called once per frame


    void Update()
    {
        //m_sizeCamera = (float)(3 + (decimal)(m_rigidbody2D.velocity.x / 10f));
        //m_veloc = m_rigidbody2D.velocity;
        if (!m_isActive)
            return;
        //
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            m_leftEnter = false;
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            m_rightEnter = false;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            m_leftEnter = true;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            m_rightEnter = true;
        }
      
    }
    private void FixedUpdate()
    {
        if (m_rightEnter)
        {
            if (m_countMove < 2)
                m_countMove++;
            m_left = -1;


        }
        else if (m_leftEnter)
        {
            if (m_countMove < 2)
                m_countMove++;
            m_left = 1;

        }
        else if (!m_rightEnter)
        {

            m_countMove--;
        }
        else if (!m_leftEnter)
        {

            m_countMove--;
        }

        if (m_countMove < 1)
        {
            m_countMove = 0;
            m_isPush = false;
        }
        else
        {
            m_isPush = true;
        }

        if (m_isPush)
        {
            m_timePush += Time.deltaTime;
            m_timeDontPush = 0;
            if (!m_isZoomCamera)
            {
                m_isZoomCamera = true;
                //m_sizeCamera = 4;
                ZoomCamera(3.5f, 4, 0.5f);
            }
        }
        else
        {
            m_timePush = 0;
            if (m_isActive)
                m_timeDontPush += Time.deltaTime;
            //m_left = 0;
        }
        //m_timePush += Time.deltaTime;

        if ((int)m_timePush == 0 && !m_isPush)
        {
            MainController.UpdateArrowRpm(0, false);
            MainController.UpdateArrowBoost(0, false);
            //
            OnMoveTo0();
            m_isMove = false;
        }
        else
        {


            UpdateRotate();
            //
            // velocity toi da > 8.5f
            int value = Mathf.Abs(m_rigidbody2D.velocity.x) > 8.5f ? 180 : 110;
            MainController.UpdateArrowRpm(value, true);
            //
            MainController.UpdateArrowBoost(value - 60, true);
            //
            OnMoveFix(m_left);

        }
        //
        if (m_groundWheelLeft && m_groundWheelRight)
        {
          
            JointSuspension2D su = new JointSuspension2D();
            su.frequency = 2.5f;
            su.angle = 270 + transform.rotation.z;
            su.dampingRatio = 0.1f;
            foreach (WheelJoint2D item in hingeJoints)
            {
                item.suspension = su;
            }
        }

        CheckFlip();


        MainController.UpdateUIDistanceFuelCanister(transform.position.x);
        MainController.UpdateScore((int)(transform.position.x - m_velocityX));
        MainController.UpdateDistanceCarLevel(((float)m_velocityX - transform.position.x) * 2);
        //
       
    }
    //private void 
    private void CheckFlip()
    {
        if (!m_groundWheelLeft && !m_groundWheelRight && m_isActive)
        {
            m_timeInAir += Time.deltaTime;
        }
        else
        {
            float time = m_timeInAir;//> 0.5 ? (int)m_timeInAir : 1;
            if (time > 0.5)
            {
                time = (int)m_timeInAir > 0.5 ? (int)m_timeInAir : 1;
                MainController.UpdateShowTextFlip(1, (int)time);
            }
            //Debug.Log(time + "==================================");
            //
            m_timeInAir = 0;
        }
        if (transform.rotation.z > 180 && m_timeInAir > 0.5f)
        {
            MainController.UpdateShowTextFlip(2, 1);
        }
        if (transform.rotation.z < -180 && m_timeInAir > 0.5f)
        {
            MainController.UpdateShowTextFlip(3, 1);
        }
    }
    private void LateUpdate()
    {
        //m_camera.orthographicSize = m_sizeCamera;
        m_camera.transform.position = new Vector3(transform.position.x + m_sizeCamera, transform.position.y, -10);
        m_cameraScreenShot.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
        //
        //Debug.Log(m_sizeCamera + "=====================================");
        m_camera.orthographicSize = Mathf.Lerp(m_camera.orthographicSize, m_sizeCamera,2f);
    }
    private void OnMoveFix(int move)
    {

        float xRight = (float)0.637661 + transform.rotation.z / 20;
        float yRight = (float)-0.5486221;
        float yLeft = (float)-0.5486221;
        float xLeft = (float)-0.637661 + transform.rotation.z / 20;
        foreach (WheelJoint2D item in hingeJoints)
        {
            if (item.connectedBody.name == m_wheelRight.name)
                item.anchor = new Vector2(xRight, yRight);
            else// if (item.connectedBody.name == m_wheelLeft.name)
                item.anchor = new Vector2(xLeft, yLeft);
        }

        float velocityCarX = m_rigidbody2D.velocity.x; //neu velocity 
        if (move * (-1) * velocityCarX > 0)// cung huong
        {

            float max = m_speedMovement * move;

            float count = 0;
            JointMotor2D motor;
            if (!m_isMove)
            {
                if (Mathf.Abs(velocityCarX) < 1.5f)
                {
                    if (m_groundWheelLeft)
                        MakeEffectCount(1, m_rigidbody2D.velocity, true, m_wheelLeft.position, transform.rotation.z);
                }
                //Debug.Log("cung chieu =======================================false");
                DOTween.To(x => count = x, 0.1f, 1, 0.5f).OnUpdate(() =>
               {
                   //if (count < 0.2f / 5f)
                   //{
                   //m_rigidbody2D.velocity = temp / 5f; // giam velocity 
                   //m_rigidbody2D.velocity = new Vector3(0, 0, 0);
                   // }
                   motor = new JointMotor2D { motorSpeed = count * max * 3, maxMotorTorque = 2800 };
                   //Debug.Log("start =============================" + motor.motorSpeed);
                   foreach (WheelJoint2D item in hingeJoints)
                   {
                       if (item.connectedBody.name == m_wheelLeft.name)
                       {
                           item.motor = motor;
                           //m_wheelLeft.AddTorque(count * m_rotate * move * (-1));
                           //m_rigidbody2D.AddTorque(count * m_rotate * move * (-1));

                       }
                   }
               }).OnComplete(() =>
               {
                   m_isMove = true;
               });
            }
            else
            {

                motor = new JointMotor2D { motorSpeed = max * 3, maxMotorTorque = 2800 };
                if (!m_groundWheelRight)
                    motor = new JointMotor2D { motorSpeed = max * 3, maxMotorTorque = 1600 };
                //Debug.Log("begin =============================" + motor.motorSpeed);
                foreach (WheelJoint2D item in hingeJoints)
                {
                    if (item.connectedBody.name == m_wheelLeft.name)
                    {
                        item.motor = motor;
                        //m_rigidbody2D.AddTorque( m_rotate * move * (-1));
                        //Debug.Log(m_rotate * move * (-1) + "=========================================");
                        //m_wheelLeft.AddTorque(count * m_rotate * move);

                    }
                }
            }
        }
        // nguoc huong -> ve 0 -> tang toc tu tu -> tang toc toi da
        else  // nguoc huong 
        {
            //Debug.Log("nguoc chieu =======================================");
            if (Mathf.Abs(velocityCarX) < 2.5f)
            {
                if (m_groundWheelLeft)
                    MakeEffectCount(1, m_rigidbody2D.velocity, false, m_wheelLeft.position, transform.rotation.z);
            }
            //Debug.Log(" =========== nguoc  huong ============== " + velocityCarX);
            if (Mathf.Abs(velocityCarX) < 2.5f) // quan tinh nho
            {
                if (!m_isMove) // vay con tang toc tu tu ko ?
                {

                    TangTocTu0(m_left, 0f, velocityCarX);

                }
                else
                {
                    DatVelocityToiDa(m_left, velocityCarX);
                }
            }
            else
            {

                // nguoc lai thi phai doi dung han roi moi di
                QuanTinhConThua(velocityCarX);

                if (!m_isMove)
                {
                    TangTocTu0(m_left, 1f, velocityCarX);
                }
                else
                    DatVelocityToiDa(m_left, velocityCarX);

            }
        }

    }

    private void QuanTinhConThua(float velocityCarX)
    {

        JointMotor2D motor;
        float quantinh = Mathf.Abs(velocityCarX) / 5f - (m_timePush + 0.1f);
        //if (velocityCarX > m_wheelLeft.velocity.x)
        //    m_wheelLeft.transform.rotation = Quaternion.identity;

        motor = new JointMotor2D { motorSpeed = 0, maxMotorTorque = 0 };
        foreach (WheelJoint2D item in hingeJoints)
        {
            if (item.connectedBody.name == m_wheelLeft.name)
            {
                item.motor = motor;
                //m_wheel1.velocity =  new Vector3(0, 0, 0);

            }
        }
        m_wheelLeft.gameObject.transform.rotation = Quaternion.identity;
        if (m_groundWheelLeft)
            MakeEffectCount(1, m_rigidbody2D.velocity, true, m_wheelLeft.position, transform.rotation.z);

        if (m_left == 1) // neu dung ben trai
        {
            m_wheelRight.gameObject.transform.rotation = Quaternion.identity;
            if (m_groundWheelRight)
                MakeEffectCount(1, m_rigidbody2D.velocity, true, m_wheelRight.position, transform.rotation.z);
        }
    }
    private void TangTocTu0(int move, float time, float velocityCarX)
    {

        float count = 0;
        JointMotor2D motor;

        //if (velocityCarX < move)
        //    m_wheelLeft.transform.rotation = Quaternion.identity;
        if (time == 0)
        {

            m_isMove = true;
            if (m_groundWheelRight && m_groundWheelLeft) // giam velocity va tang goc ban dau
            {
                if (m_left == 1)
                {
                    //Debug.Log(" tang toc tuc thoi ==========");
                    m_rigidbody2D.velocity = new Vector3(velocityCarX / 1.2f, m_rigidbody2D.velocity.y / 1.2f, 0);
                    m_rigidbody2D.AddTorque(-100f * 5);
                }
                else
                {
                    //Debug.Log(" tang toc tuc thoi ==========");
                    m_rigidbody2D.AddTorque(250f);
                }
            }
        }
        else
        {
            DOTween.To(x => count = x, 0, 1, time).OnUpdate(() =>
            {
                motor = new JointMotor2D { motorSpeed = count * 3, maxMotorTorque = 2800 };
                //Debug.Log("start =============================" + motor.motorSpeed);
                foreach (WheelJoint2D item in hingeJoints)
                {
                    if (item.connectedBody.name == m_wheelLeft.name)
                    {
                        item.motor = motor;
                        //m_wheelLeft.AddTorque(count * m_rotate * move);
                    }
                }
            }).OnComplete(() => {
                m_isMove = true;
                //Debug.Log("============dem ============" + count11);
            });
        }
    }
    private void DatVelocityToiDa(float move, float velocityCarX)
    {
        //if (velocityCarX > m_wheelLeft.velocity.x)
        //    m_wheelLeft.transform.rotation = Quaternion.identity;
        float max = m_speedMovement * move;
        JointMotor2D motor;

        motor = new JointMotor2D { motorSpeed = max * 3, maxMotorTorque = 2800 };

        //Debug.Log("begin =============================" + motor.motorSpeed);
        foreach (WheelJoint2D item in hingeJoints)
        {
            if (item.connectedBody.name == m_wheelLeft.name)
            {
                item.motor = motor;
                //m_wheelLeft.AddTorque(count * m_rotate * move);
            }
        }
    }

    private void UpdateRotate()
    {
        JointSuspension2D su = new JointSuspension2D();
        su.frequency = 2.5f;
        su.angle = 270 + transform.rotation.z;
        su.dampingRatio = 0.1f;
        if (m_groundWheelLeft && m_groundWheelRight) // check co cham dat -> rotate yeu
        {
            foreach (WheelJoint2D item in hingeJoints)
            {
                item.suspension = su;
            }
            //m_rigidbody2D.AddTorque(m_rotate * m_left * (-1));
        }
        else
        {
            //Debug.Log(" run auto compl�te");
            Rigidbody2D rigid = m_wheelLeft;
            if (!m_groundWheelLeft && !m_groundWheelRight)
            {
                su.frequency = 10f;
                //
                if (!m_groundWheelRight)
                    rigid = m_wheelRight;
                foreach (WheelJoint2D item in hingeJoints)
                {
                    if (item.attachedRigidbody == rigid)
                        item.suspension = su;
                }
                m_rigidbody2D.AddTorque(50f * m_left * (-1));
            }
            else
                m_rigidbody2D.AddTorque(30f * m_left * (-1));
        }

    }
    private void OnMoveTo0()
    {
        JointMotor2D motor;
        motor = new JointMotor2D { motorSpeed = 0, maxMotorTorque = 0 };
        foreach (WheelJoint2D item in hingeJoints)
        {
            item.motor = motor;
        }
        //if (m_timeDontPush > 0.8f) // neu dang o tren khong thi banh xe trc nang hon
        //{
        //    // m_wheelRight.gravityScale = 2.5f;

        //}
    }
    private void MakeEffectCount(int count, Vector2 velocity, bool isQuanTinh, Vector2 position, float angle)
    {

        int i = 0;
        while (i < count)
        {
            StartCoroutine(MakeEffect(i, 0.01f, velocity, isQuanTinh, position, angle));
            i++;
        }
    }
    private IEnumerator MakeEffect(int i, float timeWait, Vector2 velocity, bool isQuanTinh, Vector2 position, float angle)
    {
        //float distance;
        yield return new WaitForSeconds(timeWait * i);
        //Debug.Log(m_isPush + "==============push");
        EffectRocks rock = Instantiate(m_effectRock);
        rock.transform.SetParent(transform.parent);
        //rock.transform.position = new Vector2( m_wheel1.gameObject.transform.position.x + m_left/5f, m_wheel1.transform.position.y);
        rock.transform.position = new Vector2(position.x + m_left / 5f, position.y);
        rock.gameObject.SetActive(true);
        //distance = Mathf.Abs(m_rigidbody2D.velocity.x) * 5f;
        rock.Init(m_left, m_timePush, velocity, isQuanTinh, angle);
    }

    IEnumerator Init()
    {
        yield return new WaitForSeconds(0.2f);
        GameObject obj = Instantiate(m_effectRock.gameObject);
        obj.transform.position = m_effectRock.transform.position;
        obj.transform.SetParent(transform.parent);
        obj.gameObject.SetActive(true);
        obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(m_force, Mathf.Abs(m_force)));
        StartCoroutine(Init());
    }

    private void UpdateCheckGroundWheel(Rigidbody2D wheelCar, bool isAdd)
    {
        m_isActive = true;
        if (isAdd)
        {
            if (wheelCar == m_wheelLeft)
                m_groundWheelLeft = true;
            else
                m_groundWheelRight = true;
        }
        else
        {
            if (wheelCar == m_wheelLeft)
                m_groundWheelLeft = false;
            else
                m_groundWheelRight = false;
        }
    }
   
    private void ZoomCamera(float pointStart, float pointEnd , float time)
    {
        float count = pointStart;
        DOTween.To(x => count = x, pointStart, pointEnd, time).OnUpdate(() =>
        {
            //Debug.Log("zoom camera===================");
            m_sizeCamera = count;
        }).SetEase(Ease.Linear);
    }
    private void GameOver()
    {
        //Debug.Log("=====debug ==============");
        MainController.UpdateShowTextFlip(4, 1);
        m_isActive = false;
        float value = m_camera.orthographicSize;
        if (m_camera.orthographicSize > 3)
             ZoomCamera(value, 3,1f);
        StartCoroutine(WaitUpdateAction());
    }
    private IEnumerator WaitUpdateAction()
    {
        yield return new WaitForSeconds(3f);
        m_rigidbody2D.bodyType = RigidbodyType2D.Static;
    }    
    public void OnLeftPointerEnter(BaseEventData eventData)
    {
        //if (!m_isActive)
        //    return;
        m_leftEnter = true;
        MainController.UpdateBntBrake(true);
    }

    public void OnRightPointerEnter(BaseEventData eventData)
    {
        //if (!m_isActive)
        //    return;
        m_rightEnter = true;
        MainController.UpdateBntGas(true);
    }
    public void OnLeftPointerExit(BaseEventData eventData)
    {
        //if (!m_isActive)
        //    return;
        m_leftEnter = false;
        MainController.UpdateBntBrake(false);
    }

    public void OnRightPointerExit(BaseEventData eventData)
    {
        //if (!m_isActive)
        //    return;
        m_rightEnter = false;
        MainController.UpdateBntGas(false);
    }
}
