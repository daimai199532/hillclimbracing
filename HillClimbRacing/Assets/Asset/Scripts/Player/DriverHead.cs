using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DriverHead : MonoBehaviour
{
    [SerializeField] private HingeJoint2D m_hingeJoint2D;
    //[SerializeField] private ScreenshotHandler m_screenshotHandlder;
    private bool m_isGameOver = false;
    private void OnEnable()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //private IEnumerator WaitGameOver()
    //{
    //    yield return new WaitForSeconds(1f);
    //    m_screenshotHandlder.TakeScreenshot();
    //    yield return new WaitForSeconds(2.5f);
    //    MainController.GameOver();
    //}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "ground")
        {
            //m_isGameOver = true;
            m_hingeJoint2D.useLimits = false;
            //JointAngleLimits2D joint = new JointAngleLimits2D();
            //joint.min = 55;
            //joint.max = -55;
            //m_hingeJoint2D.limits = joint;
           
            //
            //StartCoroutine(WaitGameOver());
            if(!m_isGameOver)
            {
                MainController.GameOver();
                m_isGameOver = true;
            }
        }
    }
}
