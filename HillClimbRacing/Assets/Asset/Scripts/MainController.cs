using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
public static class MainController 
{

    public static Action<Rigidbody2D, bool> updateCheckGroundWheelCar;
    //
    public static Action<int> updateCoinEvent;
    public static Action<int> updateDiamondEvent;
    public static Action<int> updateTimeEvent;
    public static Action<int, bool> updateArrowRpmEvent;
    public static Action<int, bool> updateArrowBoostEvent;
    public static Action<int> updateScoreEvent;
    public static Action<float> updateDistanceCar;
    public static Action<bool> updateBntGasEvent;
    public static Action<bool> updateBntBrakeEvent;
    //
    public static Action gameStartEvent;
    public static Action gameOverEvent;
    //
    public static Action<float, bool> updateFuelCanisterStartGame;
    public static Action<float> updateUIDistanceFuelCanister;
    public static Action<int, int> updateShowTextFlip;
    public static void InitData()
    {
        MainModel.LoadData();
        UpdateCoin(0);
        UpdateTime(0);
        UpdateDiamond(0);
    }
    public static void UpdateCoin(int Coin)
    {
        MainModel.UpdateCoin(Coin);
        updateCoinEvent?.Invoke(Coin);
    }
    public static void UpdateTime(int Time)
    {
        updateTimeEvent?.Invoke(Time);
    }
    public static void UpdateDiamond(int Diamond)
    {
        MainModel.UpdateDiamond(Diamond);
        updateDiamondEvent?.Invoke(Diamond);
    }
    public static void UpdateArrowRpm(int value, bool isTang)
    {
        //Debug.Log(value + "============================ArrowRpm");
        updateArrowRpmEvent?.Invoke(value, isTang);
    }
    public static void UpdateArrowBoost(int value, bool isTang)
    {
        updateArrowBoostEvent?.Invoke(value, isTang);
    }
    public static void UpdateScore(int score)
    {
        if (score > MainModel.scoreLevel)
            MainModel.UpdateScore(score);
        updateScoreEvent?.Invoke(score);
    }
    public static void UpdateDistanceCarLevel(float value)
    {
        MainModel.UpdateDistanceLevel(value);
        updateDistanceCar?.Invoke(value);
    }
    public static void UpdateBntGas(bool isActive)
    {
        updateBntGasEvent?.Invoke(isActive);
    }
    public static void UpdateBntBrake(bool isActive)
    {
        updateBntBrakeEvent?.Invoke(isActive);
    }
    public static void GameOver()
    {
        gameOverEvent?.Invoke();
    }
    public static void GameStart() // bat scene GamePlay -> Load lai data level -> 
    {
        SceneManager.LoadScene(1);
        gameStartEvent?.Invoke();
        //InitData();
    }

    // Xu ly Car 
    public static void UpdateCheckGroundWheelCar( Rigidbody2D wheelCar, bool isAdd)
    {
        updateCheckGroundWheelCar?.Invoke(wheelCar, isAdd);
    }
    
    public static void UpdateFlip(int value)
    {
        MainModel.UpdateFlip(value);
    }
    public static void UpdateAirTime(int value)
    {
        MainModel.UpdateAirTime(value);
    }
    public static void UpdateBackFlip(int value)
    {
        MainModel.UpdateBackFlip(value);
    }
    public static void UpdateNeckFlip(int value)
    {
        MainModel.UpdateNeckFlip(value);
    }
    public static void UpdateTotalLevelMilestone(int value)
    {
        if (value > MainModel.totalLevelMilestone)
            MainModel.UpdateTotalLevelMilestone(value);
    }
    //
    public static void UpdateFuelCanisterStartGame(float position, bool isAdd)
    {
        updateFuelCanisterStartGame?.Invoke(position, isAdd);
    }
    public static void UpdateUIDistanceFuelCanister(float value)
    {
        updateUIDistanceFuelCanister?.Invoke(value);
    }
    public static void UpdateShowTextFlip(int index, int value)
    {
        updateShowTextFlip?.Invoke(index, value);
    }
}
