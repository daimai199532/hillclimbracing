using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelSetting : MonoBehaviour
{
    private void OnEnable()
    {
        Time.timeScale = 0;
    }
    private void OnDisable()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }
    public void RestartOnClick()
    {
        SceneManager.LoadScene(1);
    }
    public void ResumeOnClick()
    {
        OnDisable();
    }
    public void HomeOnClick()
    {
        SceneManager.LoadScene(0);
    }
}
