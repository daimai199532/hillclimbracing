using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class PanelGameOver : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI m_title;
    [SerializeField] TextMeshProUGUI m_diamondText;
    [SerializeField] TextMeshProUGUI m_coinText;
    [SerializeField] TextMeshProUGUI m_distanceText;
    [SerializeField] TextMeshProUGUI m_airTimeText;
    [SerializeField] TextMeshProUGUI m_backFlipText;
    [SerializeField] TextMeshProUGUI m_flipText;
    [SerializeField] TextMeshProUGUI m_neckFlipText;
    [SerializeField] TextMeshProUGUI m_restartText;
    [SerializeField] GameObject m_diamond;
    [SerializeField] GameObject m_coin;
    [SerializeField] GameObject m_distance;
    [SerializeField] GameObject m_airTime;
    [SerializeField] GameObject m_backFlip;
    [SerializeField] GameObject m_flip;
    [SerializeField] GameObject m_neckFlip;
    [SerializeField] Button m_restart;
    [SerializeField] ScreenshotHandler m_screenshot;
    [SerializeField] GameObject m_contentLeft;
    //
    [Header("Content Left")]
    [SerializeField] TextMeshProUGUI m_distanceLeftText;
    [SerializeField] TextMeshProUGUI m_typeMapLeftText;
    [SerializeField] TextMeshProUGUI m_highScoreLeftText;
    [SerializeField] TextMeshProUGUI m_boosterLeftText;
    [SerializeField] TextMeshProUGUI m_airTimeLeftText;
    private void OnEnable()
    {
        //Time.timeScale = 1;
        Init();
        OnShowProperties();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void OnRestartBnt()
    {
        //MainController.GameStart();
        //gameObject.SetActive(false);
        SceneManager.LoadScene("GamePlay");
    }
    private void Init() // hien - fade hien mo + xoay 
    {
        //int distance = Mathf.Abs((int)MainModel.distanceLevel);
        m_distanceText.text = "DISTANCE: " + MainModel.scoreLevel + "m";
        m_coinText.text = "+"+ MainModel.coinLevel.ToString()+ " COINS";
        m_diamondText.text = "+" + MainModel.diamondLevel.ToString() + " GEMS";
        //
        m_flipText.text = MainModel.flipLevel + "xFLIP";
        m_airTimeText.text = MainModel.airTimeLevel + "xAIR TIME";
        m_backFlipText.text = MainModel.backFlipLevel + "xBACK FLIP";
        m_neckFlipText.text = MainModel.neckFlipLevel + "xNECK FLIP";
        //
        m_distanceLeftText.text = MainModel.scoreLevel + "m";
        m_typeMapLeftText.text = MainModel.typeMapLevel;
        m_highScoreLeftText.text ="(best: "+ MainModel.hightScore+ "m)";
        m_boosterLeftText.text = MainModel.gliderBoosterLevel + "xGLIDER BOOSTER";
        m_airTimeLeftText.text = MainModel.airTimeLevel + "xAIR TIME";
        //
        m_screenshot.GetPictureAndShowIt();
    }
   
    private void OnShowProperties()
    {

        StartCoroutine(WaitScreenShot());
        //
        Sequence seq = DOTween.Sequence();
        seq.PrependCallback(() => {
            m_title.gameObject.SetActive(true);
        });
        seq.Append(m_title.transform.DORotate(new Vector3(0, 0, 0), 0.25f)).OnComplete(() => {
           
        });
        seq.Join(m_title.DOFade(1f, 0.25f));
        //
        seq.AppendInterval(0.15f).AppendCallback(() => {
            m_distance.SetActive(true);
        });
        seq.Append(m_distanceText.transform.DORotate(new Vector3(0, 0, 0), 0.15f));
        seq.Join(m_distanceText.DOFade(1f, 0.15f));
        //
        seq.AppendInterval(0.15f).AppendCallback(() => {
            m_diamond.SetActive(true);
        });
        seq.Append(m_diamondText.transform.DORotate(new Vector3(0, 0, 0), 0.15f));
        seq.Join(m_diamondText.DOFade(1f, 0.15f));

        //
        seq.AppendInterval(0.15f).AppendCallback(() => {
            m_coin.SetActive(true);
        });
        seq.Append(m_coinText.transform.DORotate(new Vector3(0, 0, 0), 0.15f));
        seq.Join(m_coinText.DOFade(1f, 0.15f));

        //
        seq.AppendInterval(0.15f).AppendCallback(() => {
            m_airTime.SetActive(true);
        });
        seq.Append(m_airTime.transform.DORotate(new Vector3(0, 0, 0), 0.15f));
        seq.Join(m_airTimeText.DOFade(1f, 0.15f));
    
        //
        seq.AppendInterval(0.15f).AppendCallback(() => {
            m_backFlip.SetActive(true);
        });
        seq.Append(m_backFlip.transform.DORotate(new Vector3(0, 0, 0), 0.15f));
        seq.Join(m_backFlipText.DOFade(1f, 0.15f));
        // m_flip
        seq.AppendInterval(0.15f).AppendCallback(() => {
            m_flip.SetActive(true);
        });
        seq.Append(m_flip.transform.DORotate(new Vector3(0, 0, 0), 0.15f));
        seq.Join(m_flipText.DOFade(1f, 0.15f));
        // m_neckFlip
        seq.AppendInterval(0.15f).AppendCallback(() => {
            m_neckFlip.SetActive(true);
        });
        seq.Append(m_neckFlip.transform.DORotate(new Vector3(0, 0, 0), 0.15f));
        seq.Join(m_neckFlipText.DOFade(1f, 0.15f));
        // m_restart
        seq.AppendInterval(0.15f).AppendCallback(() => {
            m_restart.gameObject.SetActive(true);
        });
        seq.Append(m_restart.transform.DORotate(new Vector3(0, 0, 0), 0.15f));
        seq.Join(m_restartText.DOFade(1f, 0.15f));
        //
        seq.Play().SetDelay(0.1f);
        //
       
    }
    IEnumerator WaitScreenShot()
    {
        //Debug.Log("screenshot ===========================");
        //m_screenshot.GetPictureAndShowIt();
        yield return new WaitForSeconds(0.1f);
        m_contentLeft.SetActive(true);
        Sequence seq = DOTween.Sequence();
        seq.Append(m_contentLeft.transform.DOScale(1f, 0.5f));
        seq.Join(m_contentLeft.transform.DORotate(new Vector3(0, 0, 0), 1f));
        seq.Play();
       
    }
}
