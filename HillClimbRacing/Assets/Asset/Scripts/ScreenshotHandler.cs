using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ScreenshotHandler : MonoBehaviour
{
    [SerializeField] private Camera m_camera;
    [SerializeField] private Image m_image;
    private bool takeScreenshotOnNextFrame = false;

    //public TextAsset image;
    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.A))
    //    {
    //        TakeScreenshot();
    //    }
    //    if (Input.GetKeyDown(KeyCode.D))
    //    {
    //        GetPictureAndShowIt();
    //    }
    //}
    public void OnPostRender()
    {
        if(takeScreenshotOnNextFrame)
        {
            takeScreenshotOnNextFrame = false;
            RenderTexture renderTexture = m_camera.targetTexture;
            Texture2D renderResult = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGB24, false);
            Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);//
            renderResult.ReadPixels(rect, 0, 0);

            byte[] byteArray = renderResult.EncodeToPNG();
            System.IO.File.WriteAllBytes(Application.persistentDataPath + "/CameraScreenshot.png", byteArray);
            RenderTexture.ReleaseTemporary(renderTexture);
            m_camera.targetTexture = null;
        }
    }
  
    public void TakeScreenshot()
    {
        m_camera.targetTexture = RenderTexture.GetTemporary(235, 205, 16);
        takeScreenshotOnNextFrame = true;

    }
    public void GetPictureAndShowIt()
    {
        Texture2D texture = null;
        byte[] fileBytes;

        fileBytes = System.IO.File.ReadAllBytes(Application.persistentDataPath + "/CameraScreenshot.png");
        texture = new Texture2D(235, 205, TextureFormat.RGB24, false);
        texture.LoadImage(fileBytes);
        if (texture == null)
            return;
        Sprite sp = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        m_image.sprite = sp;//
    }

}
