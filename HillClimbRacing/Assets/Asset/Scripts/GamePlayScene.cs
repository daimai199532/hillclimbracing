using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GamePlayScene : MonoBehaviour
{
    [SerializeField] private ScreenshotHandler m_screenshotHandlder;
    [SerializeField] private Camera m_camera;
    [SerializeField] private TextMeshProUGUI m_textPropertiesFill;
    [SerializeField] private TextMeshProUGUI m_textTargetInLevel;
    [SerializeField] private TextMeshProUGUI m_textCoin;
    [SerializeField] private Transform m_parentTextCoin;
    [SerializeField] private TextMeshProUGUI m_textDiamond;
    [SerializeField] private Transform m_parentTextDiamond;
    [SerializeField] private TextMeshProUGUI m_textHighScore;
    [SerializeField] private TextMeshProUGUI m_textMilestore;
    [SerializeField] private TextMeshProUGUI m_textScoreLevel;
    [SerializeField] private TextMeshProUGUI m_textDistanceFuel;
    [SerializeField] private Image m_processTime;
    [SerializeField] private Image m_imageScreenshotHandlder;
    [SerializeField] private Animation m_noticeFuel;
    [SerializeField] private GameObject m_arrowRpm;
    [SerializeField] private GameObject m_highScore;
    [SerializeField] private GameObject m_milestone;
    [SerializeField] private GameObject m_arrowBoost;
    [SerializeField] private GameObject m_panelGameStart;
    [SerializeField] private GameObject m_panelGameOver;
    [SerializeField] private GameObject m_panelControll;
    [SerializeField] private GameObject m_panelPropertite;
    [SerializeField] private GameObject m_panelSetting;
    //
    [SerializeField] private RectTransform m_contentDistance;
    [SerializeField] private RectTransform m_imagePrefabDistance;
    [SerializeField] private RectTransform m_imagePrefabArrowMilestore;
    [SerializeField] private RectTransform m_imagePrefabArrowHightScore;
    [SerializeField] private Image m_bntGas;
    [SerializeField] private Image m_bntBrake;
    [SerializeField] private Sprite[] m_gas = new Sprite[2];
    [SerializeField] private Sprite[] m_brakes = new Sprite[2];
    private int m_angleArrowRpm = 120;
    private int m_angleArrowBoost = 120;

    private List<RectTransform> m_listArrowMilestone = new List<RectTransform>();
    private List<int> m_listValueMilestone = new List<int>() {200, 600, 1600, 2000};
    private RectTransform m_arrowHightScore;
    private Tweener m_runTime;

    private List<float> m_listObjectFuelCanister = new List<float>();

    private bool m_isHighScore = false;
    private int m_countHighScore = 0;
    private void OnEnable()
    {
        DOTween.KillAll();
        Application.targetFrameRate = 60;
        //
        MainController.updateCoinEvent += UpdateCoin;
        MainController.updateDiamondEvent += UpdateDiamond;

        MainController.updateArrowRpmEvent += OnUpdateArrowRpm;
        MainController.updateArrowBoostEvent += OnUpdateArrowBoost;
        MainController.updateScoreEvent += OnUpdateScore;
        MainController.updateDistanceCar += OnUpdateDistance;
        MainController.gameOverEvent += GameOver;
        MainController.gameStartEvent += GameStart;
        MainController.updateTimeEvent += OnRunProcessTime;
        MainController.updateBntGasEvent += OnUpdateBntGas;
        MainController.updateBntBrakeEvent += OnUpdateBntBrake;
        //
        MainController.updateFuelCanisterStartGame += OnUpdateFuelCanisterStartGame;
        MainController.updateUIDistanceFuelCanister += OnUpdateUIDistanceFuelCansiter;
        MainController.updateShowTextFlip += ShowTextPropertiesFill;
        //
        MainController.InitData();

        InitDistanceUI();

        GameStart();
        OnRunProcessTime(0);
        //
     
        //
        DOTween.PlayAll();
    }
    private void OnDisable()
    {
        MainController.updateCoinEvent -= UpdateCoin;
        MainController.updateDiamondEvent -= UpdateDiamond;
        MainController.updateTimeEvent -= OnRunProcessTime;
        MainController.updateArrowRpmEvent -= OnUpdateArrowRpm;
        MainController.updateArrowBoostEvent -= OnUpdateArrowBoost;
        MainController.updateScoreEvent -= OnUpdateScore;
        MainController.updateDistanceCar -= OnUpdateDistance;
        MainController.gameOverEvent -= GameOver;
        MainController.gameStartEvent -= GameStart;
        MainController.updateBntGasEvent -= OnUpdateBntGas;
        MainController.updateBntBrakeEvent -= OnUpdateBntBrake;
        //
        MainController.updateFuelCanisterStartGame -= OnUpdateFuelCanisterStartGame;
        MainController.updateUIDistanceFuelCanister = OnUpdateUIDistanceFuelCansiter;
        MainController.updateShowTextFlip -= ShowTextPropertiesFill;

        DOTween.KillAll();
    }
   
 
    private void GameStart()// timeScale = 0 -> On/off( panel) -> SetUp Properties ->
    {
        
        m_panelGameOver.SetActive(false);
        //m_panelGameStart.SetActive(true);
        //
        MainController.UpdateCoin(0);
        MainController.UpdateDiamond(0);
        StartCoroutine(ShowText(MainModel.totalLevelMilestone + 1, m_listValueMilestone[MainModel.totalLevelMilestone], 1f, false));
    }
    private void OnUpdateFuelCanisterStartGame(float position, bool isAdd)
    {
        if(isAdd)
        {
            if (!m_listObjectFuelCanister.Contains(position))
                m_listObjectFuelCanister.Add(position);
        }
        else
        {
            m_listObjectFuelCanister.Remove(position);
        }
        m_listObjectFuelCanister.Sort();
      
    }
    private void UpdateCoin(int Coin)
    {
        //m_totalCoin += Coin;
        m_textCoin.text = MainModel.totalCoin.ToString();
        m_parentTextCoin.DOScale(1.15f, 0.05f).OnComplete(() =>
        {
            m_parentTextCoin.DOScale(1f, 0.1f).OnComplete(() => {

            });
        });

    }
   
    private void UpdateDiamond(int diamond)
    {
        //m_totalCoin += diamond;
        m_textDiamond.text = MainModel.totalDiamond.ToString();
        m_parentTextDiamond.DOScale(1.15f, 0.05f).OnComplete(() =>
        {
            m_parentTextDiamond.DOScale(1f, 0.1f).OnComplete(() => {

            });
        });
    }

    private void OnRunProcessTime(int Time)
    {
        if (m_runTime != null)
            m_runTime?.Kill();
        float count = 1;
        float timeMax = 30f;
        if (Time > 0)
        {
            timeMax = Time;
            count = 1;
            if (m_runTime != null)
            {
                m_runTime = DOTween.To(x => count = x, 1, 0, timeMax).SetEase(Ease.Linear).OnUpdate(() =>
                {
                    if (count > 0.6f)
                    {
                        m_processTime.color = Color.green;

                    }
                    else if (count > 0.3f)
                    {
                        m_processTime.color = Color.yellow;//new Color32(255, 118, 0, 255);
                    }
                    m_processTime.fillAmount = count;
                    if (count < 0.3 && count > 0.02)
                    {
                        m_processTime.color = new Color32(255, 0, 25, 255);
                        RunNoticeFuel(true);
                    }
                    else
                        RunNoticeFuel(false);
                    //if (count < 0.1 && timeMax > 1)
                    //{
                    //    MainController.GameOver();
                    //}
                }).OnComplete(() => {
                    //Debug.Log("dotween ==================");
                    MainController.GameOver();
                }); ;
            }
        }
        else
            m_runTime = DOTween.To(x => count = x, 1, 0, timeMax).SetEase(Ease.Linear).OnUpdate(() =>
            {
                m_processTime.fillAmount = count;
                if (count > 0.6f)
                {
                    m_processTime.color = Color.green;
                }
                else if (count > 0.3f)
                {
                    m_processTime.color = Color.yellow;//new Color32(255, 118, 0, 255);
                }
                if (count < 0.3 && count > 0.02)
                {
                    RunNoticeFuel(true);
                    m_processTime.color = Color.red;//new Color32(255, 0, 25, 255);
                }
                else
                    RunNoticeFuel(false);
                //if (count < 0.1 && timeMax > 1)
                //{
                    
                //}

            }).OnComplete(() => {
                //Debug.Log("dotween ==================");
                MainController.GameOver();
            });

        if (m_processTime == null)
            return;

       
    }
    private void RunNoticeFuel(bool isAction)
    {

        m_noticeFuel.gameObject.SetActive(isAction);
        m_noticeFuel.enabled = isAction;
    }
    private void OnUpdateArrowRpm(int value, bool isTang)
    {
        // tang == angleDefault + value
        // giam == angle
        //Debug.Log(value + "============================ArrowRpm");
        if(isTang)
            m_arrowRpm.transform.DORotate(new Vector3(0, 0, m_angleArrowRpm - value), 1f).SetEase(Ease.OutBack);
        else
            m_arrowRpm.transform.DORotate(new Vector3(0, 0, m_angleArrowRpm), 1f).SetEase(Ease.OutBack);
    }
    private void OnUpdateArrowBoost(int value, bool isTang)
    {
        // tang == angleDefault + value
        // giam == angle
        //Debug.Log(value + "============================ArrowRpm");
        if (isTang)
            m_arrowBoost.transform.DORotate(new Vector3(0, 0, m_angleArrowBoost - value), 2f).SetEase(Ease.InBounce);
        else
            m_arrowBoost.transform.DORotate(new Vector3(0, 0, m_angleArrowBoost), 2f).SetEase(Ease.InBounce);
    }
    private void OnUpdateScore(int value) // value = x2
    {
        //
        m_textScoreLevel.text = MainModel.scoreLevel + "m";
        //
        int index = MainModel.totalLevelMilestone;
      
        if (value + 350 > MainModel.hightScore)
        {
            m_highScore.SetActive(false);
        }
       if(index + 1 < m_listValueMilestone.Count) // dieu kien value qua list
        {
            if (value + 350 < m_listValueMilestone[index])
            {
                m_milestone.SetActive(true);
            }
            else
            {
                m_milestone.SetActive(false);
            }
        }
    }
    private void InitDistanceUI()
    {
        //m_contentDistance.gameObject.SetActive(false);
        int x = 200;//285;// khoang cach moi image
        m_textScoreLevel.text = MainModel.scoreLevel + "m";
       
        //

        //Debug.Log("===hightScore=============" + MainModel.hightScore);
        for (int i=0; i < 40;i++)
        {
            RectTransform image = Instantiate(m_imagePrefabDistance);
            image.SetParent(m_contentDistance.transform,false);
            //image.anchoredPosition = new Vector3(i * x - 100 - 200, 0, 0);
            image.anchoredPosition = new Vector3(i*x-200-100, m_contentDistance.anchoredPosition.y, 0);
            image.gameObject.SetActive(true);
        }

        //
        for (int i = 0; i < m_listValueMilestone.Count; i++)// make arrow Milestone
        {
            int milestone = m_listValueMilestone[i]; //1 bang 100m
            RectTransform image1 = Instantiate(m_imagePrefabArrowMilestore);
            image1.SetParent(m_contentDistance.transform, false);
            image1.anchoredPosition = new Vector3((milestone-100)*2 , m_contentDistance.anchoredPosition.y - 25f, 0);
            m_listArrowMilestone.Add(image1);
        }
        //
        if(m_listArrowMilestone.Count > 0)
            m_listArrowMilestone[MainModel.totalLevelMilestone].gameObject.SetActive(true);
        //
        //Debug.Log("=================== done "+ MainModel.hightScore);
        if (MainModel.hightScore > 0) // make arrow highScore
        {
           
            m_arrowHightScore = Instantiate(m_imagePrefabArrowHightScore);
            m_arrowHightScore.SetParent(m_contentDistance.transform, false);
            m_arrowHightScore.anchoredPosition = new Vector3(MainModel.hightScore - 100, m_contentDistance.anchoredPosition.y - 25f, 0);
            m_arrowHightScore.gameObject.SetActive(true);
        }
        //
        m_contentDistance.DOScale(1, 1f).OnComplete(()=> {
            m_contentDistance.gameObject.SetActive(true);
        });
        //
        if (m_listValueMilestone.Count > 1) // on/off icon Milestore start game
        {
            //Debug.Log("================" + MainModel.totalLevelMilestone);
            if (m_listValueMilestone[MainModel.totalLevelMilestone] > 350)
            {
                m_textMilestore.text = m_listValueMilestone[MainModel.totalLevelMilestone].ToString();
                m_milestone.SetActive(true);
            }
        }

        
    }
    private void OnUpdateDistance(float value)
    {

        m_contentDistance.anchoredPosition = new Vector3(MainModel.distanceLevel, 0, 0);
        //
        //Debug.Log(value + "==============");
        int index = (int)(Mathf.Abs(MainModel.distanceLevel) / 2f);

        //Debug.Log(m_arrowHightScore.anchoredPosition.x + "==============");
        //Debug.Log(MainModel.hightScore + "==============" + index);
        if (m_listValueMilestone[MainModel.totalLevelMilestone] < index)
        {
            MainController.UpdateTotalLevelMilestone(MainModel.totalLevelMilestone + 1);
            //
            m_textMilestore.text = m_listValueMilestone[MainModel.totalLevelMilestone].ToString();
            for (int j = 0; j < m_listArrowMilestone.Count - 1; j++)
            {
                m_listArrowMilestone[j].gameObject.SetActive(false);
            }
            m_listArrowMilestone[MainModel.totalLevelMilestone].gameObject.SetActive(true);
            StartCoroutine(ShowText(MainModel.totalLevelMilestone + 1, m_listValueMilestone[MainModel.totalLevelMilestone], 0.05f,true));
        }
        //
        if((index * 2)-100 > MainModel.hightScore)
        {
            MainModel.UpdateHightScore(index);
            if (m_arrowHightScore != null)
            {
                //Debug.Log("============Done ===================");
                m_arrowHightScore.gameObject.SetActive(false);
                if(m_countHighScore < 1)
                   StartCoroutine(ShowTextHighScore());
            }
        }
        //
       
        ////
        //Debug.Log(index + "==============");
        
    }
    private void OnUpdateBntGas(bool isActive)
    {
        if (isActive)
            m_bntGas.sprite = m_gas[1];
        else
            m_bntGas.sprite = m_gas[0];
    }
    private void OnUpdateBntBrake(bool isActive)
    {
        if (isActive)
            m_bntBrake.sprite = m_brakes[1];
        else
            m_bntBrake.sprite = m_brakes[0];
    }

    private IEnumerator WaitGameOver()
    {
        yield return new WaitForSeconds(1.5f);
        m_imageScreenshotHandlder.DOFade(1, 0.01f);
        yield return new WaitForSeconds(2.5f);
        m_screenshotHandlder.TakeScreenshot();
        //m_screenshotHandlder.GetPictureAndShowIt();
        m_imageScreenshotHandlder.gameObject.SetActive(true); // Hieu ung chup man hinh
        m_imageScreenshotHandlder.DOFade(0, 0.8f).OnComplete(() => {
            m_imageScreenshotHandlder.gameObject.SetActive(false);
        });
        //
        //m_screenshotHandlder.GetPictureAndShowIt();
        yield return new WaitForSeconds(1f);
        m_panelControll.SetActive(false);
        m_panelPropertite.SetActive(false);
        m_panelGameOver.SetActive(true);
        if (m_runTime != null)
            m_runTime?.Kill();
        //Debug.Log("GameOver ===============================");
    }
    private void GameOver()
    {
        StartCoroutine(WaitGameOver());
        //m_panelGameOver.SetActive(true);
    }
    public void StartGame()
    {
        //MainController.InitData();
        //InitDistanceUI();
        m_panelGameStart.SetActive(false);
        Time.timeScale = 1;
    }
    public void SettingOnClick()
    {
        m_panelSetting.SetActive(true);
        //GameStart();
        //SceneManager.LoadScene("GamePlay");
        //Time.timeScale = 1;
        //GameOver();
        //MainController.GameOver();
    }
    IEnumerator ShowTextHighScore()
    {
        m_countHighScore = 1;
        m_isHighScore = true;
        m_textTargetInLevel.DOFade(1, 0.01f);
        m_textTargetInLevel.transform.DOScale(1, 0.01f);
        m_textTargetInLevel.text = "NEW HIGHSCORE";
        //
        m_textTargetInLevel.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        m_isHighScore = false;
        m_textTargetInLevel.gameObject.SetActive(false);
    }

    IEnumerator ShowText(int level, int distance, float time , bool isUpMistone)
    {
        if(isUpMistone)
        {
            float timeWait = 0.01f;
            if (m_isHighScore)
                timeWait = 1.5f;
            yield return new WaitForSeconds(timeWait);
            m_textTargetInLevel.DOFade(1, 0.01f);
            m_textTargetInLevel.transform.DOScale(1, 0.01f);
            m_textTargetInLevel.text = "LEVEL " + level + " COMPLETE ";
            //
            m_textTargetInLevel.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            m_textTargetInLevel.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.5f);
            m_textTargetInLevel.text = "Level " + level + " bonus ";
            //
            m_textTargetInLevel.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            m_textTargetInLevel.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.5f);
            //
            m_textTargetInLevel.text = "Level bonus +" + (3500).ToString();
            m_textTargetInLevel.gameObject.SetActive(true);
            yield return new WaitForSeconds(1f);
            float value = 0;
            DOTween.To(x => value = x, 6, 26, 2f).OnUpdate(() => {
                m_textTargetInLevel.text = "Level bonus +" + ((int)value * 500).ToString();
            }).OnComplete(() => {
                m_textTargetInLevel.gameObject.SetActive(false);
            });

            //
            yield return new WaitForSeconds(2.1f);
            m_textTargetInLevel.DOFade(1, 0.01f);
            m_textTargetInLevel.text = "Level " + level + ": " + "Reach " + distance + "m";
            m_textTargetInLevel.transform.localScale = new Vector3(0, 0, 0);
            //m_textTargetInLevel.transform.Rotate(new Vector3(0, 0, 45));
            yield return new WaitForSeconds(time);
            m_textTargetInLevel.gameObject.SetActive(true);
            m_textTargetInLevel.transform.DOScale(1, 0.8f).SetEase(Ease.InQuad);


            yield return new WaitForSeconds(3f);
            m_textTargetInLevel.transform.DOScale(1.5f, 0.5f).SetEase(Ease.InQuad);
            m_textTargetInLevel.DOFade(0, 0.5f);
            yield return new WaitForSeconds(0.6f);
            m_textTargetInLevel.gameObject.SetActive(false);
        }
        else
        {
            //
            yield return new WaitForSeconds(0.1f);
            m_textTargetInLevel.DOFade(1, 0.01f);
            m_textTargetInLevel.text = "Level " + level + ": " + "Reach " + distance + "m";
            m_textTargetInLevel.transform.localScale = new Vector3(0, 0, 0);
            //m_textTargetInLevel.transform.Rotate(new Vector3(0, 0, 45));
            yield return new WaitForSeconds(time);
            m_textTargetInLevel.gameObject.SetActive(true);
            m_textTargetInLevel.transform.DOScale(1, 0.8f).SetEase(Ease.InQuad);


            yield return new WaitForSeconds(3f);
            m_textTargetInLevel.transform.DOScale(1.5f, 0.5f).SetEase(Ease.InQuad);
            m_textTargetInLevel.DOFade(0, 0.5f);
            yield return new WaitForSeconds(0.6f);
            m_textTargetInLevel.gameObject.SetActive(false);
        }
       
    }
    private void OnUpdateUIDistanceFuelCansiter(float value)
    {
        //Debug.Log(m_listObjectFuelCanister[0] + "==============" + value * 2f);
        if (m_listObjectFuelCanister.Count < 1)
            return;
        int distanceFuel = (int)(Mathf.Abs(value) - Mathf.Abs(m_listObjectFuelCanister[0]));
        if(distanceFuel < 1 || distanceFuel > 60)
        {
            m_textDistanceFuel.text = "";
        }
        else
        {
            m_textDistanceFuel.text = distanceFuel.ToString() + "m";
        }
        
    }
    private void ShowTextPropertiesFill(int index, int value)
    {
        StartCoroutine(WaitShowTextPropertiesFill(index, value));
    }
    private IEnumerator WaitShowTextPropertiesFill(int index, int value)
    {
        yield return new WaitForSeconds(0.01f);
        switch (index)
        {
            case 1: // air Time
                MainController.UpdateAirTime(value);
                m_textPropertiesFill.text = "AIR TIME +" + (value * 25).ToString(); 
                break;
            case 2: // flip
                MainController.UpdateFlip(value);
                m_textPropertiesFill.text = "FLIP +" + value.ToString();
                break;
            case 3: // back flip
                MainController.UpdateBackFlip(value);
                m_textPropertiesFill.text = " BACK FLIP +" + value.ToString();
                break;
            case 4: // neck flip
                MainController.UpdateNeckFlip(value);
                m_textPropertiesFill.text = " NECK FLIP +" + value.ToString();
                break;

        }
        yield return new WaitForSeconds(0.1f);
        m_textPropertiesFill.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        m_textPropertiesFill.gameObject.SetActive(false);
    }
}
