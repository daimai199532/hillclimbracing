
using UnityEngine;
public static class MainModel 
{
    public static int totalLevelMilestone;
    public static int totalCoin;
    public static int totalDiamond;
    public static int milestore;
    public static int hightScore;
    public static int scoreLevel; // distance car going
    public static float distanceLevel;// bieu dien UI di chuyen tren thanh conten Distance
    public static int coinLevel;
    public static int diamondLevel;
    public static int airTimeLevel;
    public static int flipLevel;
    public static int backFlipLevel;
    public static int neckFlipLevel;
    public static string typeMapLevel;
    public static int gliderBoosterLevel;

    public static void LoadInit()
    {
        //totalCoin = 0;
        //totalDiamond = 0;
    }
    public static void LoadData()
    {
        milestore = 500;
        scoreLevel = 0;
        distanceLevel = 0;
        coinLevel = 0;
        diamondLevel = 0;
        typeMapLevel = "CONTRYSIDE";
        //
        totalCoin = PlayerPrefs.GetInt("coin", 0);
        totalDiamond = PlayerPrefs.GetInt("diamond", 0);
        hightScore = PlayerPrefs.GetInt("hightScore", 0);
        totalLevelMilestone = PlayerPrefs.GetInt("totalLevelMilestone", 0);
    }
    public static void SaveData()
    {
        PlayerPrefs.SetInt("coin", totalCoin);
        PlayerPrefs.SetInt("diamond", totalDiamond);
        //PlayerPrefs.SetInt("milestore", milestore);
        PlayerPrefs.SetInt("hightScore", hightScore);
        PlayerPrefs.SetInt("totalLevelMilestone", totalLevelMilestone);
        //
        PlayerPrefs.Save();
    }
    public static void UpdateCoin(int coin)
    {
        coinLevel += coin;
        totalCoin += coin;
        SaveData();
    }
    public static void UpdateDiamond(int diamond)
    {
        diamondLevel += diamond;
        totalDiamond += diamond;
        SaveData();
    }
    public static void UpdateHightScore(int score)
    {
        hightScore = score;
        SaveData();
    }
    public static void UpdateTotalLevelMilestone(int level)
    {
        totalLevelMilestone = level;
        SaveData();
    }
    public static void UpdateScore(int score)
    {
        scoreLevel = score;
    }
    public static void UpdateDistanceLevel( float value)
    {
        distanceLevel = value ;
    }
    public static void UpdateFlip(int value)
    {
        flipLevel += value;
    }
    public static void UpdateAirTime(int value)
    {
        airTimeLevel += value;
    }
    public static void UpdateBackFlip(int value)
    {
        backFlipLevel += value;
    }
    public static void UpdateNeckFlip(int value)
    {
        neckFlipLevel += value;
    }
}
